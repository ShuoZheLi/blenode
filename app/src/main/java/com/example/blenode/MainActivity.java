package com.example.blenode;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertisingSet;
import android.bluetooth.le.AdvertisingSetCallback;
import android.bluetooth.le.AdvertisingSetParameters;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.ParcelUuid;
import android.util.Log;
import android.widget.Toast;
import java.nio.charset.Charset;
import java.util.List;
import java.util.UUID;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    String msg = "BleNode: ";

    // left for further development on identification purpose
//    private BluetoothDevice thisDevice;
//    private BluetoothDevice targetDevice;

    // custom UUID for our service
    // **may need to be changed later according to the BT association standard**
    private UUID targetUUID = UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");

    // enable the BT service
    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    // for scanning the other node
    private BluetoothLeScanner bluetoothLeScanner;
    private List<BluetoothGattService> mServiceList;

    // for broadcasting signal and establishing server
    private BluetoothLeAdvertiser advertiser =
            BluetoothAdapter.getDefaultAdapter().getBluetoothLeAdvertiser();
    private AdvertisingSet currentAdvertisingSet;
    private BluetoothManager mBluetoothManager;
    BluetoothGattServer server;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(msg, "The onCreate() event");
        // check if device support BT
        if(mBluetoothAdapter == null){
            Toast.makeText(this, "No support for BT", Toast.LENGTH_SHORT).show();
        }else{
            enableBT();
        }
    }

    // method enable Bluetooth
    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void enableBT(){
        // case: bluetooth already enabled
        if(mBluetoothAdapter.isEnabled()){
            Toast.makeText(this, "Bluetooth already enabled", Toast.LENGTH_SHORT).show();
        }else{
            // case: bluetooth not enabled yet
            Toast.makeText(this, "enabling Bluetooth", Toast.LENGTH_SHORT).show();
            Intent enableBtIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 1);
        }
        // start advertising
        enableAdvert();
        // start Gatt Server
        startGattServer();
        // start scanning for devices
        scanLeDevice();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void enableAdvert(){
        // set the parameter for advertising signal
        // https://developer.android.com/reference/android/bluetooth/le/AdvertisingSetParameters
        AdvertisingSetParameters parameters = (new AdvertisingSetParameters.Builder())
                .setLegacyMode(true)
                .setConnectable(true)
                .setScannable(true)
                .setInterval(AdvertisingSetParameters.INTERVAL_HIGH)
                .setTxPowerLevel(AdvertisingSetParameters.TX_POWER_MEDIUM)
                .build();
        // set what data to be Advertised (cannot be too large)
        AdvertiseData data = (new AdvertiseData.Builder()).setIncludeDeviceName(true).build();

        // call back for response to the advertising
        // https://developer.android.com/reference/android/bluetooth/le/AdvertisingSetCallback
        AdvertisingSetCallback callback = new AdvertisingSetCallback() {
            @Override
            public void onAdvertisingSetStarted(AdvertisingSet advertisingSet, int txPower, int status) {
                Log.d(msg, "#####onAdvertisingSetStarted(): txPower:" + txPower + " , status: "
                        + status);
                currentAdvertisingSet = advertisingSet;
                // After onAdvertisingSetStarted callback is called, you can modify the
                // advertising data and scan response data:
                advertisingSet.setAdvertisingData(new AdvertiseData.Builder().
                        setIncludeDeviceName(true).setIncludeTxPowerLevel(true).build());
            }

            @Override
            public void onAdvertisingDataSet(AdvertisingSet advertisingSet, int status) {
                Log.d(msg, "#####onAdvertisingDataSet() :status:" + status);
            }

            @Override
            public void onScanResponseDataSet(AdvertisingSet advertisingSet, int status) {
                Log.i(msg, "onScanResponseDataSet(): status:" + status);
            }

            @Override
            public void onAdvertisingSetStopped(AdvertisingSet advertisingSet) {
                Log.i(msg, "onAdvertisingSetStopped():");
            }
        };

        // start Advertising
        advertiser.startAdvertisingSet(parameters, data, null, null, null, callback);
        // When done with the advertising:
        // advertiser.stopAdvertisingSet(callback);
    }

    //===========================================================================================
    // start gatt server for responding read/write operations and make notification
    protected void startGattServer(){
        // BT manager for gatt server
        mBluetoothManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);

        // call back for response to read/write operations and make notification
        //https://developer.android.com/reference/android/bluetooth/BluetoothGattServerCallback
        BluetoothGattServerCallback bluetoothGattServerCallback= new BluetoothGattServerCallback() {

            @Override
            public void onConnectionStateChange(BluetoothDevice device, int status, int newState) {
                super.onConnectionStateChange(device, status, newState);
            }

            @Override
            // response to read operation from the scanner
            public void onCharacteristicReadRequest(BluetoothDevice device, int requestId, int offset, BluetoothGattCharacteristic characteristic) {
                Log.i(msg, "onCharacteristicReadRequest():");
                // obtain the time stamp
                Long tsLong = System.currentTimeMillis()/1000;
                String ts = tsLong.toString();
                characteristic.setValue(ts.getBytes( Charset.forName( "UTF-8" ) ));
                // response to the Gatt read request
                server.sendResponse(device,requestId, BluetoothGatt.GATT_SUCCESS,offset,characteristic.getValue());
                super.onCharacteristicReadRequest(device, requestId, offset, characteristic);
            }

            @Override
            // response to write operation
            public void onCharacteristicWriteRequest(BluetoothDevice device, int requestId, BluetoothGattCharacteristic characteristic, boolean preparedWrite, boolean responseNeeded, int offset, byte[] value) {
                super.onCharacteristicWriteRequest(device, requestId, characteristic, preparedWrite, responseNeeded, offset, value);
            }

            @Override
            // response to read request for descriptor of characteristic
            public void onDescriptorReadRequest(BluetoothDevice device, int requestId, int offset, BluetoothGattDescriptor descriptor) {
                super.onDescriptorReadRequest(device, requestId, offset, descriptor);
            }

            @Override
            // response to write request for descriptor of characteristic
            public void onDescriptorWriteRequest(BluetoothDevice device, int requestId, BluetoothGattDescriptor descriptor, boolean preparedWrite, boolean responseNeeded, int offset, byte[] value) {
                super.onDescriptorWriteRequest(device, requestId, descriptor, preparedWrite, responseNeeded, offset, value);
            }
        };

        // start the Gatt Server for responding read/write operations and make notification
        server= mBluetoothManager.openGattServer(this, bluetoothGattServerCallback);

        // create service
        BluetoothGattService service = new BluetoothGattService(targetUUID, BluetoothGattService.SERVICE_TYPE_PRIMARY);

        // create charactertic
        BluetoothGattCharacteristic characteristic = new BluetoothGattCharacteristic(targetUUID,
                BluetoothGattCharacteristic.PROPERTY_READ | BluetoothGattCharacteristic.PROPERTY_WRITE |
                        BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                BluetoothGattCharacteristic.PERMISSION_READ | BluetoothGattCharacteristic.PERMISSION_WRITE);
        // attach Descriptor to the charactertic
        characteristic.addDescriptor(new BluetoothGattDescriptor(targetUUID, BluetoothGattCharacteristic.PERMISSION_WRITE));
        // add Characteristic to service
        service.addCharacteristic(characteristic);
        // add service to the server
        server.addService(service);
    }

    //===========================================================================================
    // part of code for scanning devices and obtain the time stamp
    @Override
    // ask for the permission of BT
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Bluetooth service has started", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "No Bluetooth permission", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    // setup and start scan BT_LE device
    private void scanLeDevice() {
        // ask user for COARSE_LOCATION Permission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},1);
        }
        Log.d(msg, "scanning devices");
        // setup scanner then start scan
        bluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        bluetoothLeScanner.startScan(callback);
    }

    // call back for dealing the scanning result
    // https://developer.android.com/reference/android/bluetooth/le/ScanCallback
    private ScanCallback callback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            // comments out for debuging purpose
            //if(result.getDevice().getName() != null){
                Log.d(msg, "the device being connected:"+result.getDevice().getName());
                // make a gatt connection to the device
                result.getDevice().connectGatt(MainActivity.this, false, mGattCallback);
            //}
            super.onScanResult(callbackType, result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
        }
    };

    //
    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

        // Callback indicating when GATT client has connected/disconnected to/from a remote GATT server
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.d(msg, "onConnectionStateChange status:" + status + "  newState:" + newState);
            if (status == 0) {
                gatt.discoverServices();
            }
        }

        // Callback invoked when the list of remote services, characteristics and descriptors for the remote device have been updated, ie new services have been discovered.
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            Log.d(msg, "onServicesDiscovered status" + status);
            mServiceList = gatt.getServices();

            // comment out for debug purpose
//            if (mServiceList != null) {
//                // comments out for debuging purpose
//                // service list in detail
//                //System.out.println(mServiceList);
//                // number of serivce on the device
//                //System.out.println("Services num:" + mServiceList.size());
//            }else{
//                Log.d(msg, "No service on the device");
//            }

            // looking for the target characteristic
            // loop for the services
            for (BluetoothGattService service : mServiceList){
                List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
                System.out.println("Service UUID：" + service.getUuid());
                // loop through the characteristic of each service
                for (BluetoothGattCharacteristic characteristic : characteristics) {
                    //gatt.setCharacteristicNotification(characteristic, true);
                    System.out.println("characteristic: " + characteristic.getUuid());

                    // check if characteristic match our target UUID
                    if(characteristic.getUuid().equals(targetUUID)){
                        // read the time stamp
                        gatt.readCharacteristic(characteristic);
                    }

                }
            }
        }

        // Callback triggered as a result of a remote characteristic notification.
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            Log.d(msg, "onCharacteristicChanged");
        }


        //Callback reporting the result of a characteristic read operation.
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            Log.d(msg, "onCharacteristicRead");
            Log.d(msg,"characteristic: " + characteristic.getUuid());
            Log.d(msg, "the time stamp"+characteristic.getStringValue(0));
            gatt.disconnect();
        }

    };

}
